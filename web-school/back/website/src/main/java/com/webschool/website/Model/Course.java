package com.webschool.website.Model;

import javax.persistence.*;
import java.util.HashSet;

@Entity
@Table(name = "course")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String courseName;

    private String topic;

    private String description;

    private String futureLessons;

    private HashSet<User> students;

    private String teacherUsername;

    private String urlPicture;

    @ManyToOne
    @JoinColumn(name = "teacher_id")
    private User teacher;

    public void setTeacher(User teacher) {
        this.teacher = teacher;
    }

    public String getTeacherUsername() {
        return teacherUsername;
    }

    public void setTeacherUsername(String teacherUsername) {
        this.teacherUsername = teacherUsername;
    }

    public User getTeacher() {
        return teacher;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFutureLessons() {
        return futureLessons;
    }

    public void setFutureLessons(String futureLessons) {
        this.futureLessons = futureLessons;
    }

    public HashSet<User> getStudents() {
        return students;
    }

    public void setStudents(HashSet<User> students) {
        this.students = students;
    }

    public String getUrlPicture() {
        return urlPicture;
    }

    public void setUrlPicture(String urlPicture) {
        this.urlPicture = urlPicture;
    }

    public Course(String courseName, String topic, String teacherUsername, String urlPicture) {
        this.courseName = courseName;
        this.topic = topic;
        this.teacherUsername = teacherUsername;
        this.urlPicture = urlPicture;
        this.students = new HashSet<User>();
    }

    public Course(String courseName, String topic, String teacherUsername) {
        this.courseName = courseName;
        this.topic = topic;
        this.teacherUsername = teacherUsername;
        this.students = new HashSet<User>();
    }

    public Course() {
        this.students = new HashSet<User>();
    }
}
