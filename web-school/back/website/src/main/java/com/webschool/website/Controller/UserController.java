package com.webschool.website.Controller;

import com.webschool.website.Model.User;
import com.webschool.website.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private UserService userService;

    @PostMapping("/create")
    public ResponseEntity<String> createUser(@RequestBody User user) {
        if (user.getUsername().length() < 6 || user.getUsername().length() > 32) {
            System.out.println("Username должен быть от 6 до 32 символов");
            return new ResponseEntity<>(
                    "Username должен быть от 6 до 32 символов",
                    HttpStatus.BAD_REQUEST);
        }
        if (userService.findByUsername(user.getUsername()) != null) {
            System.out.println("Такой username уже существует");
            return new ResponseEntity<>(
                    "Такой username уже существует",
                    HttpStatus.BAD_REQUEST);
        }

        if (user.getPassword().length() < 8 || user.getPassword().length() > 32) {
            System.out.println("Пароль должен быть от 8 до 32 символов");
            return new ResponseEntity<>(
                    "Пароль должен быть от 8 до 32 символов",
                    HttpStatus.BAD_REQUEST);
        }

        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            System.out.println("Пароли не совпадают");
            return new ResponseEntity<>(
                    "Пароли не совпадают",
                    HttpStatus.BAD_REQUEST);
        }

        user.setPassword(bCryptPasswordEncoder().encode(user.getPassword()));
        user.setPasswordConfirm(bCryptPasswordEncoder().encode(user.getPasswordConfirm()));

        userService.save(user);
        System.out.println("Аккаунт создан");
        return new ResponseEntity<>(
                "Аккаунт создан",
                HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody User user) {
        User userByUsername = userService.findByUsername(user.getUsername());

        if (userByUsername == null || !(userByUsername.getUsername().equals(user.getUsername()) &&
                bCryptPasswordEncoder().matches(user.getPassword(), userByUsername.getPassword()))) {
            System.out.println("Неправильный логин или пароль");
            return new ResponseEntity<>(
                    "Неправильный логин или пароль",
                    HttpStatus.BAD_REQUEST);
        }
        System.out.println("Вы успешно зашли");
        return new ResponseEntity<>(
                "Вы успешно зашли",
                HttpStatus.OK);
    }
}
