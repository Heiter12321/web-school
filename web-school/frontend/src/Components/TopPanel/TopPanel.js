import React from "react";
import { Link } from "react-router-dom";

export default function TopPanel() {
    return(
        <>
            <div style={{width: "100%", height: "10%", backgroundColor: "#021524", gridRow: 2, gridColumn: 1, display: "flex"}}>
                <Link to="/start_page" style={{marginLeft: "1%", marginTop: "0.4%", textDecoration: "none", width: "5%"}}>
                    <img title="Главная страница" src={"https://www.webschool.co.il/uploads/ba172695-3603-4873-9be4-79064580c803.png"} style={{width: "140%", height: "90%", marginLeft: "1%", marginTop: "0.4%"}} alt={"WebSchoolLogo"}/>
                </Link>

                <ul style={{display: "flex", marginTop: "1%"}}>
                    <li style={{display: "flex", marginLeft: "0%"}}>
                        <Link to={"/courses"} style={{textDecoration: "none"}}>
                            <span style={{color: "white", fontSize: "18px", display: "flex"}}>Kурсы</span>
                        </Link>
                    </li>
                    <li style={{display: "flex", marginLeft: "10%"}}>
                        <Link to={"/teaching"} style={{textDecoration: "none"}}>
                            <span style={{color: "white", fontSize: "18px", display: "flex"}}>Преподавание</span>
                        </Link>                  
                    </li>
                </ul>
                <div style={{width: "100%"}} />

                <ul style={{display: "flex", marginRight: "5%", marginTop: "1%"}}>
                    <li style={{display: "flex", marginLeft: "10%"}}>
                        <Link to={"/login"} style={{textDecoration: "none"}}>
                            <span style={{color: "white", fontSize: "18px", display: "flex"}}>Войти</span>
                        </Link>                      
                    </li>
                    <li style={{display: "flex", marginLeft: "10%"}}>
                        <Link to={"/registration"} style={{textDecoration: "none"}}>
                            <span style={{color: "white", fontSize: "18px", display: "flex"}}>Регистрация</span>
                        </Link>                      
                    </li>
                </ul>
            </div>
        </>
    )
}