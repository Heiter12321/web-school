import React, { useState, useEffect, useCallback } from "react"
import CourseCards from "../Components/CourseCards/CourseCards";
// import CreateCard from "../Components/CreateCard/CreateCard";
import TopPanel from "../Components/TopPanel/TopPanel";
import {PlusOutlined} from "@ant-design/icons"
import { Button, Modal, Form, Input, notification } from 'antd';

export default function Courses() {
    const [courses, setCourses] = useState({});
    
    useEffect(() => {
        fetch("http://localhost:8080/course/getAllCourses", {
            method: "GET",
            headers: { 'Content-Type': 'application/json' }
        })
        .then(response => 
        response.json()
        ).then (r => {
            setCourses({ r });
        })
    }, [setCourses]);

    const CollectionCreateForm = ({ visible, onCreate, onCancel }) => {
        const [form] = Form.useForm();
        return (
          <Modal
            visible={visible}
            title="Создание нового курса"
            okText="Создать"
            cancelText="Отмена"
            onCancel={onCancel}
            onOk={() => {
              form
                .validateFields()
                .then((values) => {
                  form.resetFields();
                  values["teacherUsername"] = "Heiter12321";
                  onCreate(values);
                  console.log(values);
                })
                .catch((info) => {
                  console.log('Validate Failed:', info);
                });
            }}
          >
            <Form
              form={form}
              layout="vertical"
              name="form_in_modal"
              initialValues={{
                modifier: 'public',
              }}
            >
              <Form.Item
                name="courseName"
                label="Название курса"
                rules={[
                  {
                    required: true,
                    message: 'Please input the name of course!',
                  },
                ]}
              >
      
                <Input />
      
              </Form.Item>
      
              <Form.Item
                name="topic"
                label="Тема курса"
                rules={[
                  {
                    required: true,
                    message: 'Please input the topic of course!',
                  },
                ]}
              >
      
                <Input />
                
              </Form.Item>
      
              <Form.Item name="description" label="Описание курса">
                <Input type="textarea" />
              </Form.Item>
      
              <Form.Item name="urlPicture" label="Ссылка на картинку для обложки курса">
                <Input type="textarea" />
              </Form.Item>
            </Form>
          </Modal>
        );
    };

    const [visible, setVisible] = useState(false);
        
    const onCreate = (values) => {
        console.log('Received values of form: ', values);
    
        fetch("http://localhost:8080/course/create", {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(values)
        })  
        .then(response => 
            {
            if (response.status === 200) {
                console.log("sozdal");
            } else {
                console.log("ploho");
                return response.text()
            }
            })
            .then(data=>{ 
                console.log(data);
                notification.open({
                message: data,
                duration: 10,
                placement: "top",
                style: {
                    width: "100%",
                    textAlign: "center",
                },
                });
            })
        
      
        setVisible(false);

        fetch("http://localhost:8080/course/getAllCourses", {
            method: "GET",
            headers: { 'Content-Type': 'application/json' }
        })
        .then(response => 
        response.json()
        ).then (r => {
            setCourses({ r });
        })
    };

      const CollectionsPage = useCallback(
        () => {
            return (
                <div>
                    <Button
                    onClick={() => {
                        setVisible(true);
                    }}
                    type="primary" icon={<PlusOutlined />} style={{marginLeft: "10%", marginTop: "1%"}}
                    >
                    Создать курс
                    </Button>
                    <CollectionCreateForm
                    visible={visible}
                    onCreate={onCreate}
                    onCancel={() => {
                        setVisible(false);
                    }}
                    />
                </div>
            )
        }, [visible],
    );

    return (
        <>
            <TopPanel />
            <div style={{width: "100%", marginTop: "3%", fontSize:30, marginBottom: "0.5%", marginLeft: "10%"}}>
                Доступные курсы
            </div>
            <CourseCards courses={courses}/>
            <CollectionsPage />
        </>
    )
}