import './App.css';
import React from "react";
import { Routes, Route, BrowserRouter, Navigate } from 'react-router-dom';
import Courses from '../Pages/Courses';
import Teaching from '../Pages/Teaching';
import Login from '../Pages/Login';
import Registration from '../Pages/Registartion';
import StartPage from '../Pages/StartPage';

function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Navigate to="/login" replace />}/>
                <Route path="/start_page" element={<StartPage />}/>
                <Route path='/courses' element={<Courses />}/>
                <Route path='/teaching' element={<Teaching />}/>
                <Route path='/login' element={<Login />}/>
                <Route path='/registration' element={<Registration />}/>
            </Routes>
            
        </BrowserRouter>
    );
}

export default App;
