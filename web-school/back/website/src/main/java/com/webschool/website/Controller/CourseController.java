package com.webschool.website.Controller;

import com.webschool.website.Model.Course;
import com.webschool.website.Model.User;
import com.webschool.website.Service.CourseService;
import com.webschool.website.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/course")
@CrossOrigin
public class CourseController {

    @Autowired
    private CourseService courseService;

    @Autowired
    private UserService userService;

    @PostMapping("/create")
    public ResponseEntity<String> createCourse(@RequestBody Course course) {

        if (courseService.findByCourseName(course.getCourseName()) != null) {
            System.out.println("Курс с таким именем уже существует");
            return new ResponseEntity<>(
                    "Курс с таким именем уже существует",
                    HttpStatus.BAD_REQUEST);
        }

        if (course.getCourseName() != null && course.getTopic() != null) {
            User user = userService.findByUsername(course.getTeacherUsername());
            if (user != null) {
                course.setTeacher(user);
            } else {
                System.out.println("Преподавателя с таким username не существует");
                return new ResponseEntity<>(
                        "Преподавателя с таким username не существует",
                        HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("Для создания курса необходимо указать Название, Описание курса и Username преподавателя");
            return new ResponseEntity<>(
                    "Для создания курса необходимо указать Название, Описание курса и Username преподавателя",
                    HttpStatus.BAD_REQUEST);
        }

        courseService.save(course);
        System.out.println("Курс создан");
        return new ResponseEntity<>(
                "Курс создан",
                HttpStatus.OK);
    }

    @GetMapping("/getAllCourses")
    public List<Course> getAllCourses() {
        return courseService.getAllCourse();
    }
}
