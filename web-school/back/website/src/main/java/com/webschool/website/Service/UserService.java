package com.webschool.website.Service;

import com.webschool.website.Model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
