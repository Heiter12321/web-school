import { Form, Input, Button, notification  } from 'antd';
import {Link} from 'react-router-dom';

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

export default function Registration() {
  const [form] = Form.useForm();

  const onFinish = (values) => {
    fetch("http://localhost:8080/user/create", {
        method: "POST",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(values)
  }).then(response => 
    {
      if (response.status === 200) {
        window.location.href = '/login';
      } else {
        return response.text()
      }
  }
  ).then(data=>{ 
      console.log(data);
      notification.open({
        message: data,
        duration: 10,
        placement: "top",
        style: {
          width: "100%",
          textAlign: "center",
        },
      });
   })
  };

  return (
    <Form {...layout} form={form} name="control-hooks" onFinish={onFinish} style={{marginTop: "15%", width: "50%", marginLeft: "auto", marginRight: "30%"}}>
      <Form.Item
        label="Username"
        name="username"
        rules={[
          {
            required: true,
            message: 'Your username should be between 6 and 32 symbols!',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="First name"
        name="firstName"
        rules={[
          {
            required: true,
            message: 'Please input your first name',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Second name"
        name="secondName"
        rules={[
          {
            required: true,
            message: 'Please input your second name',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: 'Your password should be between 8 and 32 symbols!',
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        label="Confirm password"
        name="passwordConfirm"
        rules={[
          {
            required: true,
            message: 'Your passwords should be equals!',
          },
        ]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item {...tailLayout} style={{marginLeft: "25%"}}>
        <Button type="primary" htmlType="submit">
          Register
        </Button>
        <Button type="default" style={{marginLeft: "5%"}}>
            <Link to={"/login"}>
                Log on
            </Link>
        </Button>
      </Form.Item>
    </Form>
  );
};