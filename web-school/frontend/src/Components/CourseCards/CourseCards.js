import React from "react";
import { Card, Row, Avatar } from "antd";

const { Meta } = Card;

export default function CourseCards(courses) {
    return(
        <>
            <Row grid={16} style={{marginLeft: "5%"}}>

                { Object.keys(courses.courses).length === 0 ? <></> : courses.courses.r.map(course => {
                    return (
                           <Card
                                style={{ width: 300, marginLeft: 20, marginTop: 20 }}
                                cover={
                                <img
                                    alt={course.courseName}
                                    src={course.urlPicture === null ? "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" : course.urlPicture}
                                    style={{width: "100%", height: 250}}
                                />
                                }
                            >
                                <Meta
                                avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
                                title={course.courseName}
                                description={course.topic}
                                />
                            </Card>
                    )
                })}
            </Row>
        </>
    )
}