import { Form, Input, Button } from 'antd';
import {Link} from 'react-router-dom';

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

export default function Login() {
  const [form] = Form.useForm();

  const onFinish = (values) => {
    fetch("http://localhost:8080/user/login", {
        method: "POST",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(values)
    })
    .then(response => 
      {
        if (response.status === 200) {
            window.location.href = '/start_page';
        } else {
            console.log("ploho");
        }
      })
  };

  return (
    <Form {...layout} form={form} name="control-hooks" onFinish={onFinish} style={{marginTop: "15%", width: "50%", marginLeft: "auto", marginRight: "30%"}}>
      <Form.Item
        label="Username"
        name="username"
        rules={[
          {
            required: true,
            message: 'Please input your username!',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item {...tailLayout} style={{marginLeft: "25%"}}>
        <Button type="primary" htmlType="submit">
            Sign in
        </Button>
        <Button type="default" style={{marginLeft: "5%"}}>
            <Link to={"/registration"}>
                Registration
            </Link>
        </Button>
      </Form.Item>
    </Form>
  );
};