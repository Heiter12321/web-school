package com.webschool.website.Service;

import com.webschool.website.Model.Course;

import java.util.List;

public interface CourseService {
    void save(Course course);

    Course findByCourseName(String courseName);

    List<Course> getAllCourse();
}
