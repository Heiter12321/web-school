import TopPanel from "../Components/TopPanel/TopPanel";
import React from "react";
import StartPageContext from "../Components/StartPageContext/StartPageContext";

export default function StartPage() {
    return (
        <>
            <TopPanel />
            <StartPageContext />
        </>
    )
}