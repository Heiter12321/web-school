import {PlusOutlined} from "@ant-design/icons"
import React, { useState } from 'react';
import { Button, Modal, Form, Input } from 'antd';

const CollectionCreateForm = ({ visible, onCreate, onCancel }) => {
  const [form] = Form.useForm();
  return (
    <Modal
      visible={visible}
      title="Создание нового курса"
      okText="Создать"
      cancelText="Отмена"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            form.resetFields();
            values["teacherUsername"] = "Heiter12321";
            onCreate(values);
            console.log(values);
          })
          .catch((info) => {
            console.log('Validate Failed:', info);
          });
      }}
    >
      <Form
        form={form}
        layout="vertical"
        name="form_in_modal"
        initialValues={{
          modifier: 'public',
        }}
      >
        <Form.Item
          name="courseName"
          label="Название курса"
          rules={[
            {
              required: true,
              message: 'Please input the name of course!',
            },
          ]}
        >

          <Input />

        </Form.Item>

        <Form.Item
          name="topic"
          label="Тема курса"
          rules={[
            {
              required: true,
              message: 'Please input the topic of course!',
            },
          ]}
        >

          <Input />
          
        </Form.Item>

        <Form.Item name="description" label="Описание курса">
          <Input type="textarea" />
        </Form.Item>

        <Form.Item name="urlPicture" label="Ссылка на картинку для обложки курса">
          <Input type="textarea" />
        </Form.Item>
      </Form>
    </Modal>
  );
};

const CollectionsPage = (courses) => {
  const [visible, setVisible] = useState(false);

  const onCreate = (values) => {
    console.log('Received values of form: ', values);

    fetch("http://localhost:8080/course/create", {
      method: "POST",
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(values)
    })  
    .then(response => 
      {
        if (response.status === 200) {
          console.log("sozdal");
        } else {
            console.log("ploho");
            return response.text()
        }
      })
    .then(data => console.log(data))

    courses["rerender"] = "true";
    
    setVisible(false);
  };

  return (
    <div>
      <Button
        onClick={() => {
          setVisible(true);
        }}
        type="primary" icon={<PlusOutlined />} style={{marginLeft: "10%", marginTop: "1%"}}
      >
        Создать курс
      </Button>
      <CollectionCreateForm
        visible={visible}
        onCreate={onCreate}
        onCancel={() => {
          setVisible(false);
        }}
      />
    </div>
  );
};

export default (courses) => <CollectionsPage />;