import React from "react";
import { Card } from "antd";

export default function StartPageContext() {
    return(
        <>
            <Card style={{marginLeft: "auto", marginRight: "auto"}}>

                <h2 style={{textAlign: "center"}}>
                    В этом разделе будут транслироваться новости из всех курсов, на которые вы подписаны
                </h2>
            </Card>
        </>
    )
}